import config from '../../config.json';
import Database from './lib/Database';
import HttpServer from "./HttpServer";

const db = new Database(config);
db.connect()
  .then(() => {
    console.log('Database connected');
    const httpServer = new HttpServer({ config, db});
    httpServer.start();
  })
  .catch(console.error);