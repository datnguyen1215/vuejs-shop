// Base URL: /api/auth/logout

const { Router } = require('express');

export default ({ config, db }) => {
  let router = Router();

  // Get the current user.
  router.get('/', (req, res) => {
    // Destroy the user's session so that they will be completely
    // logged out.
    req.session.destroy();

    res.json({ success: true });
  })

  return router;
}