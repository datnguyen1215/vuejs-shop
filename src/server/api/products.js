// Base URL: /api/products/

const { Router } = require('express');

export default ({ config, db }) => {
  let router = Router();

  router.get('/', (req, res) => {
    return db.getProducts()
     .then(data => res.json({ success: true, data: data }))
     .catch(err => res.json({ error: err.message }));
   });

  router.get('/:id', (req, res) => {
    let { id } = req.params;

    db.getProduct(id)
      .then(data => {
        if (!data)
          return res.json ({ error: "Product doesn't exist"});

        res.json({ success: true, data })
      })
    .catch(err => res.json({ error: err.message }))
  });

  router.post('/', (req, res) => {
    let details = req.body;
    db.addProduct(details)
      .then(data => { 
        req.session.product = data;
        res.json({ success: true, data: data})
        
      })
      .catch(err => res.json({ error: err.message}));
  })

  return router;
};


