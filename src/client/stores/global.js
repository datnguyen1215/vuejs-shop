const { reactive, readonly } = require('vue');

const data = reactive({
  user: null
})

export const state = readonly(data);

export const updateUser = user => {
  data.user = user;
}