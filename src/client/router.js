import { createRouter, createWebHistory } from 'vue-router';
import Login from './pages/Login.vue';
import Home from './pages/Home.vue';
import Dashboard from './pages/Dashboard.vue';
import CreateAccount from './pages/CreateAccount.vue';
import AddProduct from './pages/AddProduct.vue';

const routes = [
  {
    name: 'home',
    path: '/',
    component: Home
  },
  {
    name: 'login',
    path: '/login',
    component: Login
  },
  {
    name: 'dashboard',
    path: '/dashboard',
    component: Dashboard
  },
  {
    name: 'profile',
    path: '/profile',
    component: Dashboard
  },
  {
    name: 'create-account',
    path: '/create-account',
    component: CreateAccount
  },
  {
    name: 'add-product',
    path: '/admin/products/add',
    component: AddProduct
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes
});

export default router;
