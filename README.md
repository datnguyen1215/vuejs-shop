# Development Guide

## How to run
After cloning the project, you can do the following command to run:
`npm install`

Change your example_config.json into config.json file and configure it to have the correct information before running:
`npm run dev`

The server can be accessed via `http://localhost:8080`.